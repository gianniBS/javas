# JavaS

# INTRODUCTION

[Ecriture du JavaScript](#ecrire-du-javascript)

[Variable & constante](#variable-et-constante)

[Nommer une variable](#nommer-une-variable)

[Les type](#les-type)

[Les operateurs](#les-operateurs)

[Strings](#strings)

[Arrays](#arrays)

[Les conditions](#les-conditions)

[Les boucles](#les-boucles)

[Les fonctions](#les-fonctions)

[Les objets](#les-objets)

[Spread operator](#spread-operator)

[Affecter par decomposition](#affecter-par-decomposition)

[Manipuler les tableaux](#manipuler-les-tableaux)

[Les classes](#les-classes)

[Manipuler le DOM](#manipuler-le-dom)

### Ecrire du JavaScript
[INTRODUCTION](#introduction)

```javascript
<script src="app.js"></script>
<script type="javascript">
console.log('ok 1');
```
---


### variable et constante
[INTRODUCTION](#introduction)


Une variable une case memoire qui contient ou non de valeur mais elle peut changer de valeur au cour du script.

Une constante ne peuvent pas changer de valeur.

Pour utiliser une variable on tape dans le fichier .js "let" (on la déclare).

Pour déclarer une constante on l'écrit const dans le .js.

Les donnée utilisée dans les constantes ou dans les variables peuvent être utilisé plus tard dans le script. 

L'interpréteur va toujours commence ligne part ligne, instruction par instruction.

On ne peux pas créer deux variables qui ont le même nom

---  


### nommer une variable
[INTRODUCTION](#introduction)


Vaut mieux mettre les noms des variables en anglais

On peut nommer une variable par des nombres allant de 0à9, de a à z en comptant aussi les majuscules et du caractère spéciale "_"

Il est preferable de ne pas commencer le nom de la variable par des nombres

Touts les mots-clefs utilisé par Js ne sont pas autorisée à être utilisé pour les noms de variables

Pour la création du nom de la variable il faut commencer par my et mettre le nom de la variable avec maj au début du mot, il faut faire en sort qui ça soit intuitif

---

### les type
[INTRODUCTION](#introduction)


```javascript 
// Number 

let age = 19;
console,log('1 :',age, typeof age,age * 3" );
 
//  String => chaîne de charactère 

let world  = "Bonjour";
 
// Boolean 

let open = true; 
open = false;

// Object 

let obj = {};
const obj = {};

// Array

const arr = [];
```

---


### Les operateurs
[INTRODUCTION](#introduction)

    
_**OPÉRATEURS DE COMPARAISON**_

- les égalités simples convertit les deux opérandes s'ils ne sont pas du même type (==).
- les égalités strict renvoie true si les opérandes sont strictement égaux (===).
- les inégalités simples renvoie true si les deux opérandes ne sont pas égaux (!=).
- les inégalités strict renvoie true si les opérandes sont de types différents ou ne sont pas égaux (!==).
- supérieur strict renvoie true si l'opérande gauche est strictement supérieur à l'opérande droit (>).
- supérieur ou égal renvoie true si l'opérande gauche est supérieur ou égal à l'opérande droit (>=).
- inférieur strict renvoie true si l'opérande gauche est strictement inférieur à l'opérande droit (<).
- inférieur ou égal renvoie true si l'opérande gauche est inférieur ou égal à l'opérande droit (<=).

<u>_**OPÉRATEURS DE LOGIQUE**_</u>


**ET logique** : le "et logique" nous permet de créer des conditions **obligatoires** (&&) :

>    console.log(expression1 && expression2);

**OU logique** : le "ou logique" nous permet de faire en sorte que si une des condition et bonne alors ça affichera true (||) :

>    console.log(expression1 || expression2); 

**NON logique** : le "non logique" nous permet d'afficher l'inverse du résultat (!):

>    ex : le résultat est "vrai" avec le "non logique" il devient "faut"

Il y a aussi un deuxième style de "non logique²" qui est l'inverse de l'inverse du résultat (!!) :

ex : le résultat est "vrai" avec le "non logique²" il devient "faut" et pour finir il se remet en "vrai" :

>    console.log(!expression1 !expression2 !expression3);

>   console.log(!!expresion1 !!expression2 !!expression3);

<u>_**OPÉRATEURS D'AFFECTATION**_</u>

<u>*Affectation simple*</u> : L'opérateur d'affectation simple permet d'assigner une valeur à une variable. Le résultat de l'affectation est la valeur affectée (nb = 30).

<u>*Affectation après addition*</u> : Cet opérateur permet d'ajouter la valeur des arguments droit à une variable, le résultat de l'addition étant affecté à cette variable (nb += 3).

<u>*Affectation après soustraction*</u> : Cet opérateur soustrait la valeur des arguments droit à la variable puis affecte le résultat de cette soustraction à la variable (nb -= 5).

<u>*Affectation après multiplication*</u> : Cet opérateur permet de multiplier une variable par la valeur des arguments droit et d'affecter le résultat de cette opération à la variable (const nb *= 2)/(nb *= 2).

<u>*Affectation après division*</u> : Cet opérateur permet de diviser une variable par la valeur des arguments droit et d'affecter le résultat de cette opération à la variable (nb /= 3).

<u>*Affectation du reste*</u> : Cet opérateur permet de diviser une variable par la valeur de l'opérande droit et d'affecter le reste de cette division à la variable (nb %= 4).

---


### Strings
[INTRODUCTION](#introduction)


Les Strings sont des chaînes de caractères 

Les Strings sont toujours entre des guillemets simple ou double ('')("")

Pour certains cas on peut échapper les caractères en mettant (\) :

>   ex : 'C'est';->'C\'est; || "C'est "Mardi";->"C'est \"Mardi\";

On peut désactiver l'échapper avec un autre (\\)

Avec _**"\n"**_ on peut sauter des lignes

Avec _**"\r"**_ on peut avencé d'un espace

On peut faire des "concaténation"(assembler deux strings ensemble) :

>    console.log(str1 + str2);

Il existe des propiéter, pour sa qu'il faut mettre un point :

>    console.log(str.length); || console.log("j'aime les".length);

**.length** affiche la valeur totale de la string sauf si on lui donne une consigne

On utilise **.toUpperCase** pour mettre un str en majuscule

La valeur du str reste la même en minuscule comme en majuscule

On utilise **.toLowerCase** pour mettre un str en minuscule

>    console.log(str.**toUpperCase**());

>   console.log(str.**toLowerCase**());

/!\ <u>**ATTENTION**</u> /!\

Toutes les majuscules son important, si on ne met pas les majuscules pour ".toUpperCase" sa ne fonctionne pas

Accéder à un caractère grace à sa position, pour sa on perd par exemple :

"Jean" la position du "J" se trouve à la position "0" et non "1" pour sa on utilise:

>    console.log(str[0]);

Si on ne connait pas la position de "n" on utilisera :

>    console.log(str[str.length -1]);
    Pourquoi "-1" parce que sans le mettre la valeur de "n" serai 4

**Indexof** sert à montrer la position d'une string dans une string, si la string dans la quel il n'y a pas le mot il affichera "-1".

---
### Arrays
[INTODUCTION](#introduction)

**Arrays** sont des tableaux, un tableau est une structure de donnée qui permet d'organiser des valeurs numéroter, il peut contenir tout type de données :

    ex: nombres, strings, objets, où même d'autes tableau qui contient d'autres tableaux

Toute valeur d'un tableau est accessible grâce a les index, l'index montre leur position dans le tableau

En **JavaScript** l'index du tableau commence toujours par "0"

On peut connaitre la longueur d'un tableau comme une strings avec ".length"

Les tableaux sont très utiles pour stocker des données comme des articles, des prénoms, des notes,etc...

Il est préférable de stocker des valeurs du même type "où de la même forme pour les objets"

<u>*CREE UN TABLEAU*</u>

On crée une const ou une variable pour faire rentre le tableau dedans puis on crée un tableau avec des "[ ]" pour ensuite insérer les valeurs en les séparant d'une virgule et d'un espace

>   const arr = [1, 2, 3, 4];
>
>   console.log(arr);

On peut faire un tableau avec plusieurs valeur identique

>   const arr2 = new Array(10).fill(10);

Ceci nous donnera un tableau avec 10 emplacements avec une valeur de 10

On peut avoir accès à la valeur grâce à son *index*, pour ça après la création du tableau marquer dans la console 

>   const arr = [0, ok, 1];
> 
>   console.log(arr[nombre: 1]);
> 
>               résultat: "ok"

Il est possible d'insérer une valeur dans le tableau

On utilise **".push()"** pour insérer une valeur dans un tableau 

>   const pro = [];
> 
>   pro.push(600);
> 
>   pro.push(400);

Il est possible de **supprimer ou de remplacer** une valeur

***ex:***
>insert
>
> const arr = [];
> 
> arr.push("Jean", "Sam", "Steve")
> 
> console.log(arr);

***supprimer:***

> arr.splice(0, 1);
> je veux sup à partir de 0 et je veux sup que 1 donc jean a été supprimer
> 
> console...

>tableau  [Jean, Sam, Steve, Pierre, Enzo, Baptiste];
> 
> tableau.splice(2, tableau.length);
> je veux sup à partir de 2 et je veux sup la longueur de tableau, il me reste "Jean, Sam"

***remplacer:***

>arr.splice(1, 2, item:"Samy");
> je veux sup à partir de 1 et je veux sup que 2 et je rajoute "Samy" donc il me reste "Jean, Samy"

---
### Les conditions
[INTRODUCTION](#introduction)

"if et else" sont des conditions : **"if" veut dire "si"**, **"else" veut dire "sinon"**

```javascript
const age = 16;
if (age >= 18) {
    console.log('Vous êtes majeur');
}else if (age >= 13 && age < 18) {
    console.log('Vous êtes un ado');    
}else {
    console.log('Vous êtes mineurs');
}
```
Le "else if" **n'es pas obligatoire**, "else if" est une contre condition

Le ***ternaire*** est un moyen d'écrire plus rapidement les conditions de "if et else"

*normal* :
```javascript
let presence = true;
if (presence === true) {
    console.log('Bonjour');
}else {
    console.log('Bye');
}
```

*le ternaire* :
```javascript
let presence = true;
    console.log((presence === true) ? 'Bonjour' : 'Bye');
```

Le ternaire peut être utilisé quand on assigne une valeur
```javascript
let message = '';
message = presence ? 'Bonjour' : 'Bye';
```

Le "**switch**" est identique aux "if, else et else if"
```javascript
let fruit = 'Oranges'
switch (fruit) {
    case "Pommes":
        console.log('Pommes 1$');
        break;
        
    default:
        console.log('Pas de fruis');
        break;        
}
```

Le "**break**" sert à casser le switch

---
### Les boucles
[INTRODUCTION](#introduction)

Les boucles permet de répéter des action "X" fois

les boucles sont représenté par le mot-clef "for"
>for(expressioninitial; condition; expressionIncremente) {
> instruction
> }

```javascript
for (let i = 0 ; i<10 ; i++); {
    console.log(i);
}
```


En **JavaScript** on attend la fin de la boucle pour executer les autres expression qui vien après la boucle

Le break fonctionne aussi sur les boucles "for"

Dans les conditions "for" peut mettre aussi par exemple une variable avec ".length"

L'instruction ***do...while*** crée une boucle qui exécute une instruction jusqu'à ce qu'une condition de test ne soit plus vérifiée. La condition est testée après que l'instruction soit exécutée, le bloc d'instructions défini dans la boucle est donc exécuté au moins une fois.
>Source : https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Instructions/do...while

>do {
>
> instruction
> 
> } while(condition)

```javascript
let i = 0;
do {
    console.log('OK');
    i++;
}while (i<10);
```

L'instruction ***while*** permet de créer une boucle qui s'exécute tant qu'une condition de test est vérifiée. La condition est évaluée avant d'exécuter l'instruction contenue dans la boucle.
>Source : https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Instructions/while

---
### Les fonctions
[INTRODUCTION](#introduction)

Les fonctions font partie des briques fondamentales de JavaScript. Une fonction est une procédure JavaScript, un ensemble d'instructions effectuant une tâche ou calculant une valeur. Afin d'utiliser une fonction, il est nécessaire de l'avoir auparavant définie au sein de la portée dans laquelle on souhaite l'appeler.
>Source : https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Fonctions

>function multiplyby(nb, nb2) {
> 
> //instruction
> 
> return"tout type de donnée qu'on veut"(nb, nb2)
> 
> }
> 
> let product = multiplyby (nb:2, nb2:4);
> 
> (ceci nous donne la valeur et invoque la fonction)
> 
> console.log(nb, nb2)

Dans les fonctions on peut aussi vérifié une donnée avec "**checkValue**", peut-être aussi utilisé dans un tableau

Les fonctions anonymes sont stocké dans des variables
>let ext = function ();

***Une fonction de rappel*** (aussi appelée callback en anglais) est une fonction passée dans une autre fonction en tant qu'argument, qui est ensuite invoquée à l'intérieur de la fonction externe pour accomplir une sorte de routine ou d'action.
>Source : https://developer.mozilla.org/fr/docs/Glossaire/Fonction_de_rappel

```javascript
function maCallback(nb) {
    return nb * 3;
}
function maFonction(nb, callback) {
    let var1 = nb * 16;
    return callback(var1);
}
let res = maFonction(nb:6, maCallback);
    console.log(res);
```

Les arguments est semblable à un tableau

---
### Les objets
[INTRODUCTION](#introduction)

On trouve souvent les objets en ***JavaScript***

un objet c'est la représentation de donnée sous forme de clé valeur

>let phone = {
> 
> key: value,
>
> key: value,
>
> key: value,
>
> }

```javascript
let phone = {
    //Propriété
    name: JP,
    marque: Ts,
    largueur: 5,
    longueur: 5,
    
    //Méthode
    call: function () {
        console.log('call Sam');
    },
    SendMsg: function (to) {
        console.log('send sms to' + to );
    },
}
console.log(phone);
```
À chaque fois qu'on sépare des propriétés ou des méthodes c'est avec des virgules

Il y a l'opérateur/mot-clé "this" que je n'ai pas compris

***this*** est une propriété du contexte d'exécution. C'est un objet spécial du contexte dans lequel le code est exécuté.

---
### Spread operator
[INTRODUCTION](#introduction)

Le ***Spread operator*** sont les "..."

Les "..." permet d'étaler un tableau, une chaîne de caractère ou un objet

*arr* :
```javascript
let arr1 = ["J", "S", "T"];
let arr2 = ["P", "L", "E"];

let arr3 = [...arr1, ...arr2];
console.log(arr3);
```

*objet* :
```javascript
let STD = {
    name: null,
    class: null,
};
let ST = {
    ...STD
};
console.log(ST.name);
```
---
### Affecter par decomposition
[INTRODUCTION](#introduction)

**L'affectation par décomposition** est une expression *JavaScript* qui permet d'extraire des données d'un tableau ou d'un objet grâce à une syntaxe dont la forme ressemble à la structure du tableau ou de l'objet
>Source : https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Op%C3%A9rateurs/Affecter_par_d%C3%A9composition

Comme pour "**Spread operator**" l'affectation par décomposition fonctionne sur les tableaux ou sur les objets

arr :
```javascript
let a, b;

let arr = [1, "J"];

[a, b] = arr;

console.log(a);
console.log(b);
```

objet :
```javascript
let name;

({name} = {name: "Jean"});

console.log('name :', name);
```

### Manipuler les tableaux
[INTRODUCTION](#introduction)

- La méthode "***.forEach()***":

    La méthode .forEach nous permet effectuer une fonction pour chaque élément d'un tableau


- La méthode "***.find()***":

    La méthode .find nous permet de trouver un élément dans un tableau en fonction d'une condition


- La méthode "***.findIndex()***":

    La méthode findIndex va retourner l'index de l'élément qui renvoie "true"


- La méthode "***.map()***":

  La méthode .map permet de modifier ou de crée un nouveau tableau à partie d'un tableau


- La méthode "***.reduce()**":

    La méthode .reduce permet de réduire un tableau à une certaine valeur où sous une autre forme


- La méthode "***.filter()***":

  La méthode .filter permet de filtrer les éléments d'un tableau en fonction d'une condition


- La méthode "***.som()e***":

  La méthode .some permet de retourner "true" si une des valeurs correspond à une des conditions


- La méthode "***.every()***":

  La méthode .every permet de retourner "true" si toutes les valeurs respectent une condition

```javascript
//.forEach:

let arr = [1, 4, 6, 8];

arr.forEach(function (value, index) {
    console.log(index, value);
});

//.find:

let arr = ["Jean", "Sam", "Steve", "Tom"];

let callback
= function (name) {
    return name.includes('o');
};
let res = arr.find(callback);
console.log('res :', res);

//.findIndex:

let arr = ["Jean", "Sam", "Steve", "Tom"];

let callback = function (element, index) {
    return element.length === 3;
};
let index = arr.findIndex(callback);

console.log('Index :', index, arr[index]);

//.map:

let arr = [1, 2, 4, 56, 78, 99, 123, 456];

let callback = function (value) {
    return value * 2;
};
let arr2 = arr.map(callback);

console.log(arr2);

//.reduce:

let arr = [3, 4, 49, 230, 48, 123];

let accumalation = arr.reduce(function (acc, value, index) {
    console.log(acc, value, index);
    
    acc = acc + value;
    
    return acc;
}, 0);

console.log(accumalation, arr)

//.filter:

let arr = [3, 4, 49, 230, 48, 123];

let arrFiltered = arr.filter(function (nb, index) {
    console.log(nb, index);
    
    return nb > 40 && nb < 200;
});

console.log('array filter :', arrFiltered);

//.some:

let arr = [3, 4, 49, 230, 48, 123];

let cond = arr.some(function (value, index) {
    console.log(value, index);
    
    return value > 200;
})

console.log(cond);

//.every:

let arr = [3, 4, 49, 230, 48, 123];

let cond = arr.every(function (value, index) {
    return value > 0;
});

console.log('Toute les valeurs :', cond);
```

---
### Les classes
[INTRODUCTION](#introduction)

Une classe est un type de fonction qui permettra de créer des objets

Les classes sont juste des fonctions spéciales. Ainsi, les classes sont définies de la même façon que les fonctions : par déclaration, ou par expression.

```javascript
class User {
    
}

let user = new User();
let user2 = new User();

user2.name = "Jean";

console.log(user, user2)
```

Les propriétés sont stockées entre les accolades "{}"

```javascript
class User {
    name = "Jean";
    age = 22;
    presence;
}
```

les variables "user" sont des objets

La méthode constructor est une méthode qui est utilisée pour créer et initialiser un objet lorsqu'on utilise le mot clé class.

Extends est utilisé dans les déclarations et expressions de classes afin de signifier qu'un type représenté par une classe hérite d'un autre type.

Static permet de définir une méthode statique d'une classe. Les méthodes statiques ne sont pas disponibles sur les instances d'une classe mais sont appelées sur la classe elle-même. Les méthodes statiques sont généralement des fonctions utilitaires (qui peuvent permettre de créer ou de cloner des objets par exemple)

On l'écrit : 
>static test() { }

### Manipuler le DOM
[INTRODUCTION](#introduction)

Le Document Object Model ou DOM (pour modèle objet de document) est une interface de programmation pour les documents HTML, XML et SVG. Il fournit une représentation structurée du document sous forme d'un arbre et définit la façon dont la structure peut être manipulée par les programmes, en termes de style et de contenu. Le DOM représente le document comme un ensemble de nœuds et d'objets possédant des propriétés et des méthodes. Les nœuds peuvent également avoir des gestionnaires d'événements qui se déclenchent lorsqu'un événement se produit. Cela permet de manipuler des pages web grâce à des scripts et/ou des langages de programmation. Les nœuds peuvent être associés à des gestionnaires d'événements. Une fois qu'un événement est déclenché, les gestionnaires d'événements sont exécutés.
>Source : https://developer.mozilla.org/fr/docs/Web/API/Document_Object_Model

Pour sélectionner un élément grâce à son "id" on utilise le fichier '.js' ou on marque 
```javascript
const ex = document.getElementById()//id de l'élément que vous souhaiter

console.log(ex);
```
on utilise les id principalement pour le JavaScript, pour manipuler les éléments avec le JavaScript et pas pour styliser les éléments

Pour styliser les éléments on va favoriser l'utilisation des classes 

On peut faire la même chose mais avec les classes
```javascript
const ex = document.getElementsByClassName()//le nom de la classe que vous souhaiter
```

Il est possible d'utiliser le DOM pour sélectionner un ou plusieurs éléments avec "document.querySelector(), si c'est avec une classe c'est avec un '.' mais si c'est avec les id alors c'est avec un '#', on peut le faire avec la balise

*le premiere élément* :
```javascript
const ex = document.querySelector('#id')
const ex = document.querySelector('.classe')
const ex = document.querySelector('balise')
```

Par exemple si l'élément a deux classes alors c'écris ".classe.classe2" et pareil pour l'id

*plusieurs éléments* :

```javascript
const ex = document.querySelectorAll('#id')
const ex = document.querySelectorAll('.classe')
const ex = document.querySelectorAll('balise')
```

Par le DOM, on peut modifié le style d'un élément en faisant :
>square.style.backgroundColor = 'yellow';

on peut faire en sort que l'élément qu'on modifie devienne dynamique avec:

###### *se qu'un exemple*
```javascript
let square;
let positionY = 0;

setInterval(function (){
    positionY++;
    square.style.top = positionY + "px";
}, 100);

console.log(square);
```

On peut créer et ajouter un élément dans du DOM
```javascript
const square = document.createElement('div');

square.classList.add(square);

square.innerHTML = '<strong>OK</strong>';

document.body.appendChild(square);

let container = document.createElement('div');

container.innerText = 'Pour du texte'

document.appendChild(container);

container.appendChild(square);

console.log(square);
```

On peut écouter les événements 

Tu crées des paramètres quand il se passe un évènement (click, molette, etc ...) c'est grâce à sa qu'on peut faire en sort qu'un site devienne dynamique

Function arrow est une expression de fonction fléchée permet d’avoir une syntaxe plus courte que les expressions de fonction et ne possède pas ses propres valeurs pour this, arguments, super, ou new.target. Les fonctions fléchées sont souvent anonymes et ne sont pas destinées à être utilisées pour déclarer des méthodes.





















